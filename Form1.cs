﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;

namespace MilesTest
{
    public partial class Form1 : Form
    {
        BindingSource bss = new BindingSource();
        List<DistanceData> lstData = new List<DistanceData>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double index = 0;
            double CarA = (5280.0 * 10.0);
            double CarB = 0;
            DistanceData newItem = null;
            do
            {
                newItem = new DistanceData()
                {
                    CarA = Math.Round(CarA, 2),
                    CarB = Math.Round(CarB, 2),
                    Index = index,
                    Minutes = Math.Round(index / 60.0, 2)
                };
                lstData.Add(newItem);
                CarA += 88.0;
                CarB += 95.3;
                index++;
            } while (CarB < CarA);
            newItem = new DistanceData()
            {
                CarA = Math.Round(CarA, 2),
                CarB = Math.Round(CarB, 2),
                Index = index,
                Minutes = Math.Round(index / 60.0, 2)
            };
            lstData.Add(newItem);


            // MessageBox.Show($"Time In Seconds: {index}");
            BindGrid();
        }

        private void BindGrid()
        {
            var bindingList = new BindingList<DistanceData>(lstData);
            var source = new BindingSource(bindingList, null);
            dgv.DataSource = source;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
