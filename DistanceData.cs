﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilesTest
{
    public class DistanceData
    {
        public double Index { get; set; }
        public double CarA { get; set; }
        public double CarB { get; set; }
        public double Minutes { get; set; }
    }
}
